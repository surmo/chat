package org.example.client;

import java.io.*;
import java.net.*;

public class Client {
    public static void main(String[] args) {
        try {
            // Устанавливаем соединение с сервером
            Socket socket = new Socket("localhost", 7077);

            // Создаем потоки для чтения и записи сообщений
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));

            // Создаем поток для чтения сообщений от сервера
            Thread readerThread = new Thread(new Reader(input));
            readerThread.start();

            // Отправляем сообщения на сервер
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String message;
            while ((message = reader.readLine()) != null) {
                output.println(message);
                output.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Поток для чтения сообщений от сервера
    private static class Reader implements Runnable {
        private BufferedReader input;

        public Reader(BufferedReader input) {
            this.input = input;
        }

        @Override
        public void run() {
            try {
                String message;
                while ((message = input.readLine()) != null) {
                    System.out.println(message);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
