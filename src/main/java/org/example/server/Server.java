package org.example.server;

import java.io.*;
import java.net.*;
import java.util.*;

public class Server {
    private static final int PORT = 7077;
    private static Set<PrintWriter> writers = new HashSet<>();

    public static void main(String[] args) throws Exception {
        ServerSocket listener = new ServerSocket(PORT);
        listener.setSoTimeout(120000);
        System.out.println("The chat server is running...");
        try {
            while (true) {
                new Handler(listener.accept()).start();
            }
        } finally {
            listener.close();
        }
    }

    private static class Handler extends Thread {
        private Socket socket;
        private String name;
        private BufferedReader in;
        private PrintWriter out;

        public Handler(Socket socket) {
            this.socket = socket;
        }

        public void run() {
            try {
                // Создаем потоки ввода/вывода для обмена данными с клиентом
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(), true);

                // Запрашиваем имя клиента и отправляем ему сообщение приветствия
                out.println("Enter your name:");
                name = in.readLine();
                out.println("Welcome to the chat, " + name + "!\n");

                // Добавляем PrintWriter текущего клиента в список
                synchronized (writers) {
                    writers.add(out);
                }

                // Читаем сообщения от клиента и отправляем их всем остальным подключенным клиентам
                String input;
                while ((input = in.readLine()) != null) {
                    synchronized (writers) {
                        for (PrintWriter writer : writers) {
                            if (writer != out) {
                                writer.println(name + ": " + input);
                            }
                        }
                    }
                }
            } catch (IOException e) {
                System.out.println(e);
            } finally {
                // Если клиент отключается, удаляем его PrintWriter из списка
                if (out != null) {
                    synchronized (writers) {
                        writers.remove(out);
                    }
                }
                try {
                    socket.close();
                } catch (IOException e) {
                    System.out.println(e);
                }
            }
        }
    }
}
